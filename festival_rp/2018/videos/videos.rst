
.. index::
   pair: Festival RP; Vidéos (2018)


.. _festival_rp_videos_2018:

==========================================
Vidéos du festival de la CNT-RP en 2018
==========================================


.. figure:: grande_salle.JPG
   :align: center


.. toctree::
   :maxdepth: 4

   refugiees/refugiees
   syndicalisme_revolutionaire/syndicalisme_revolutionaire
