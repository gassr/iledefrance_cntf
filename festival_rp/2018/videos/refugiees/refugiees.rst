
.. index::
   pair: Nil; Vidéo (2018)
   pair: CNT; Réfugiées


.. _refugiees_2018:

======================================================================================
Vidéo sur le thème "Réfugiées" de la CNT-RP en 2018
======================================================================================

.. contents::
   :depth: 4


Intervention de Nil
========================

- https://www.youtube.com/watch?v=IrszI2kJzgU&feature=youtu.be

.. figure:: nil/nil.png
   :align: center

   Nil
