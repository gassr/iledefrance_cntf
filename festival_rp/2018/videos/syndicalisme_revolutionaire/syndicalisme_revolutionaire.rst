
.. index::
   pair: Syndicalisme révolutionnaire; Vidéo (2018)

.. _syndical_rp_2018:

======================================================================================
Vidéos sur le thème "Syndicalisme révolutionnaire et révolution" de la CNT-RP en 2018
======================================================================================

.. seealso::

   - https://www.youtube.com/watch?v=uLsky0Cs_QU (partie1)
   - https://www.youtube.com/watch?v=v4YmQaZKZ-8 (partie2)
   - https://www.youtube.com/watch?v=Gujp7OdbYeo (partie3)
   - https://www.youtube.com/watch?v=Moyx7c3DFBk (partie4)


.. toctree::
   :maxdepth: 4

   partie_1/partie_1
   partie_2/partie_2
   partie_3/partie_3
   partie_4/partie_4
