
.. index::
   pair: Jérôme; Vidéo (2018)

.. _syndical_rp_2018_partie_4:

========================================================
Interventions partie 4
========================================================

.. seealso::

   - https://www.youtube.com/watch?v=Moyx7c3DFBk


Jérôme: ne connaissait rien de la CNT (30 secondes)
=====================================================

.. seealso::

   - https://youtu.be/7ulLhNJeQXc


.. figure:: jerome.png
   :align: center

   Jérôme un jeune camarade cénétiste


Il y a 2 ans je ne savais pas ce qu'on y faisait dans un syndicat, je ne
savais pas comment adhérer à un syndicat, j'ai découvert ça sur le tas ...

Exemple de livret d'accueil
-----------------------------

.. seealso:: https://cnt-f.gitlab.io/livret_accueil/livret/livret.html



Aimable: final
==========================

.. seealso::

   - https://youtu.be/228eVi7Gk7o


Voir

- :ref:`syndical_rp_2018_partie_1`
- :ref:`syndical_rp_2018_partie_3`
