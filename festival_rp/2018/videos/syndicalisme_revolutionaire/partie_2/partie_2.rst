
.. index::
   pair: Sandrine; Vidéo (2018)
   pair: Aimable; Vidéo (2018)
   pair: Annie; Vidéo (2018)
   pair: David; Vidéo (2018)
   pair: David; DUP
   pair: David; Hemmersbach
   pair: David; Section syndicale




.. contents::
   :depth: 4


.. _syndical_rp_2018_partie_2:

===================================================
Interventions partie 2 (23 minutes et 23 secondes)
===================================================

.. seealso::

   - https://www.youtube.com/watch?v=v4YmQaZKZ-8


Aimable: Présentation de David (2 minutes)
============================================

.. seealso::

   - https://youtu.be/JvAeJA58cIo


David: DUP chez Hemmersbach (7 minutes)
=========================================================

.. seealso::

   - https://youtu.be/Ksl0d3rZuj8


Situation
-----------

Seul dans l'entreprise Hemmersbach.


Outil employé : DUP (Délégation Unique du Personnel)
-------------------------------------------------------

.. seealso::

   - https://www.service-public.fr/particuliers/vosdroits/F1775
   - https://fr.wikipedia.org/wiki/D%C3%A9l%C3%A9gation_unique_du_personnel


Liens
-----

.. seealso::

   - http://www.cnt-f.org/urp/tracts/on-est-tous-des-rhinoceros


Avenir
-------

Etre 2 pour créer une section syndicale


Aimable: La section syndicale (2 minutes)
=================================================

.. seealso::

   - https://youtu.be/mzm_c7XYaNw
   - :ref:`article_premier_statuts_CNT_2014`
   - http://www.cnt-f.org/urp/section-syndicale


- esprit d'initiative et d'entreprise **pour le bien commun** et collectif
- :ref:`article 1 de la CNT <article_premier_statuts_CNT_2014>`
- se former pour la future société


Jean: Le travail ingrat mais exaltant du syndicalisme (6 minutes)
===================================================================

.. seealso::

   - https://youtu.be/a44lJ1RkWCU


.. figure:: jean.png
   :align: center

   Jean

Le syndicalisme est un travail ingrat mais c'est exaltant.
Construire un avenir meilleur à construire dès maintenant.

Où
----

A Choisy le Roi et plus largement le Val de Marne.
David est venu contacter.

Rôle du syndicat
-----------------

Adhérer à un syndicat c'est adhérer à une structure qui permet
d'avoir une **réflexion collective**.
Annie est venue elle aussi au syndicat.

La Rolse: un patron qui refuse de payer les heures supplémentaires !
Une heure travaillée doit être une heure payée.
C'est un patron voyou.


Liens Radio Libertaire
-------------------------

.. seealso::

   - http://ecoutez.radio-libertaire.org/radiolib.m3u
   - https://www.radio-libertaire.net/
   - https://www.federation-anarchiste.org/rl/prog/


Les syndicats de la CNT animent une émission sur Radio libertaire les mardis
de 20h30 à 22h30

- 1er mardi : CNT-RP
- 2e mardi : CNT éducation
- 4e mardi : Sévices publics (Énergie)

La CNT 94 anime le 5e dimanche l’émission Micro-Ondes 94 de 15h30 à 17 heures


Annie: la section syndicale chez Rolse (4 minutes")
============================================================

.. seealso::

   - https://youtu.be/cUXlow7Ukrs
   - http://www.cnt-f.org/urp/auteur/cnt-etpics-94


.. figure:: annie.png
   :align: center

   Sandrine, Aimable, Annie, David




Pourquoi ?
------------

- un patron voyou qui refuse de payer les heures


Comment lutter ?
------------------

- la grève
- aller aux prudhommes (15 mai 2018)

Problème
---------

- payer l'avocat


Résultats
----------

- visite médicale


Les traitres
--------------

- le délégué de la CGT (DRH adjoint de Rolse) qui propose à Annie un poste
  ailleurs


Liens
------

.. seealso::

   - http://www.cnt-f.org/urp/communiques-de-la-cnt/rolse-les-salariees-dernieres-roues-du-carrosse


Vidéo du 22 février 2017
-------------------------

.. seealso::

   - https://www.youtube.com/watch?v=T4W1PVKfcXs&feature=youtu.be


.. figure:: rolse.jpeg
   :align: center


Autre vidéo en le 22 février 2017
-----------------------------------

.. seealso::

   - https://www.youtube.com/watch?v=T4W1PVKfcXs&feature=youtu.be





Aimable: précisions sur Rolse (2 minutes + 2 minutes)
==============================================================================

1ere sous partie
----------------

.. seealso::

   - https://youtu.be/5AUvpZ13mc4
   - http://www.cnt-f.org/urp/auteur/cnt-etpics-94


Recueil d'informations sur Rolse.


2e sous partie
----------------

.. seealso::

   - https://youtu.be/Gj2GFLpIUXc

.. figure:: ../partie_3/aimable.png
   :align: center

   Sandrine, Aimable, Annie, David, Jean


Remarques sur Rolse
---------------------

- Annie n'a aucun mandat aux institutions réprésentatives du personnel
- Annie est représentante depuis 3 ans représentante de la section syndicale,
  ne participe pas aux élections
- faire confiance aux collègues
- le droit de grève existe, le droit de licencier aussi



Voir

- :ref:`syndical_rp_2018_partie_1`
- :ref:`syndical_rp_2018_partie_3`
