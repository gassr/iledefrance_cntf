
.. index::
   pair: Raphaël; Vidéo (2018)
   pair: Sandrine; Vidéo (2018)
   pair: Aimable; Vidéo (2018)
   pair: Annie; Vidéo (2018)
   pair: Jean; Vidéo (2018)
   pair: Section; Montreuil (2018)


.. _syndical_rp_2018_partie_1:

====================================================
Interventions partie 1 (23 minutes et 23 secondes)
====================================================



.. seealso::

   - https://www.youtube.com/watch?v=uLsky0Cs_QU


Aimable : introduction et présentation des camarades intervenantEs
====================================================================

.. seealso::

   - https://www.youtube.com/watch?v=8vo-M2mMRkg



Raphaël: Pourquoi faire un débat sur l'anarchosyndicalime ?
============================================================

.. seealso::

   - https://youtu.be/jzqJyhrK2uQ


.. figure:: raphael.png
   :align: center

   Raphaël et Sandrine


Un cap: projet de transformation révolutionnaire de la société.
Garder le cap: il n'y a pas de sauveur suprême.

Aimable "Quelles pratiques pouvons nous mettre en avant ?"
============================================================

.. seealso::

   - https://youtu.be/xqIJ89DzkA0


.. figure:: aimable_annie.png
   :align: center

   Sandrine, Aimable et Annie



Sandrine:  Pourquoi se syndiquer ?
=====================================

.. seealso::

   - https://youtu.be/RRWioq5Knbg


.. figure:: sandrine.png
   :align: center

   Raphaël, Sandrine, Aimable, Annie


Où
----

- fonction publique à la mairie de Montreuil


Motivations
-------------

- besoin de ne plus être isolé
- envie d'appartenir à un groupe
- une collectivité est une entreprise
- 14 ans de ville de Montreuil: un patron reste un patron
- section à 2 puis 4
- luttes contre la perte des acquis sociaux
- des choses à dire, rédaction d'un bulletin

Comment lutter
--------------

- diffusion d'inforùations: tracts
- la section est rattachée à un syndicat qui est une ressource (partage
  de connaissance)

Exemple de lutte
-----------------

- nettoyage des locaux en souffrance extrême
- débrayage durant 3 semaines par 200 agents:

  - => beaucoup d'écoles fermées

Résultats
++++++++++

- plus de stagiaires
- contrats de 1 an au lieu de 6 mois
- reprise du plan de formation
- remplacement des congés de maternité


Voir

- :ref:`syndical_rp_2018_partie_4`
- :ref:`syndical_rp_2018_partie_2`
