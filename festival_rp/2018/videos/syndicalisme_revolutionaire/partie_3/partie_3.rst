
.. index::
   pair: Raphaël; Vidéo (2018)
   pair: Sandrine; Vidéo (2018)
   pair: Aimable; Vidéo (2018)
   pair: Annie; Vidéo (2018)
   pair: David; Vidéo (2018)
   pair: Jean; Vidéo (2018)

.. _syndical_rp_2018_partie_3:

===================================================
Interventions partie 3 (23 minutes et 23 secondes)
===================================================


.. seealso::

   - https://www.youtube.com/watch?v=Gujp7OdbYeo


.. contents::
   :depth: 4




Aimable: la grève générale, fédérations d'industrie (2 minutes)
==================================================================

.. seealso::

   - https://youtu.be/nDDeCwSkVgM

.. figure:: aimable.png
   :align: center

   Sandrine, Aimable, Annie, David, Jean



La grève générale
-------------------

Une revendication.
Le moyen pour arrêter le bulldozer libéral.


Solidarité interpro
---------------------

- syndicat d'industrie
- section syndicale en lien avec son syndicat


Répression syndicale
----------------------

- à people and baby



Sandrine: solidarité entre syndicats de la CNT (2 minutes)
================================================================

.. seealso::

   - https://youtu.be/_dHCGGDxiaU


.. figure:: sandrine.png
   :align: center

   Sandrine, Aimable


Solidarité entre syndicats de la CNT
--------------------------------------

- coopération interprofessionnel

- Informations données aux usagers


Annie: se taire sur les injustices c'est devenir un esclave (1 minute)
===============================================================================

.. seealso::

   - https://youtu.be/_Oq1m-xt-FI

Quand il y a de l'injustice que son lieu de travail, il ne faut pas se taire.
Quand on se tait on devient comme des esclaves.


Aimable: syndicalisme révolutionnaire (2 minutes)
======================================================

.. seealso::

   - https://youtu.be/yNMgODOu5G4


- aucun militant de la CNT n'est rémunéré
- luttes conduites par les travailleurs/travailleuses elles mêmes
- garder le cap:

  - organisation de classe

La CNT exprime la possibilité de résurgence de l'anarcho-syndicalisme.

Références
-----------

- 1914
- 1936


.. _serge_interpro:

Serge de la CNT educ Drôme un exemple d'interprofessionnalisme (1 minute)
===========================================================================

.. seealso::

   - https://youtu.be/tiMjeE5Fr-w


::

    http://www.cnt-f.org/cnt-stp-26_.html




.. figure:: drome.png
   :align: center

   Serge, cénétiste de la Drôme


.. figure:: twit_serge_drome.png
   :align: center

   https://twitter.com/syndicat_CNT/status/1023207118860636161


Des lycéens ont adhéré au syndicat de l'éducation dans la Drôme.
Le rapport entre profs et élèves a changé.

David: son secteur de travail (1 minute)
==================================================

.. seealso::

   - https://youtu.be/GiTCmWKt8r8


- gestion des imprimantes
- problème de poudre blanche


Yves: comment avoir le rapport de force ?  (3 minutes)
===============================================================

.. seealso::

   - https://youtu.be/iz3YMAXTE9o


Adhérer à la CNT ca fout la trouille.


Réponse de raphaël: analyse par secteur (5 minutes)
============================================================


.. seealso::

   - https://youtu.be/Ix3SIOaxE2M


- Impossible dans certaines petites boites.
- combat international:

  - groupe de travail par continent


Réponse de Jean: réfléchir à l'accueil des gens de la CNT (1 minute)
=============================================================================


.. seealso::

   - https://youtu.be/RuQfHczut8M
   - https://cnt-f.gitlab.io/livret_accueil/livret/livret.html


- réfléchir à l'accueil des gens de la CNT
- on n'est pas un syndicat de service

Exemple de livret d'accueil
-----------------------------

.. seealso:: https://cnt-f.gitlab.io/livret_accueil/livret/livret.html



Voir

- :ref:`syndical_rp_2018_partie_2`
- :ref:`syndical_rp_2018_partie_4`
