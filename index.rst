
.. index::
   pair: Union régionale ; Région parisienne

.. figure:: images/bandoconf.jpg
   :align: center


.. figure:: images/repartition_egalitaire_des_richesses.jpg
   :align: center

.. _union_regionale_ile_de_france:

==============================================
CNT **Union Régionale Ile de France** (RP)
==============================================

- http://cnt-f.org
- :ref:`congres_cnt_f`
- :ref:`glossaire_cnt`
- :ref:`glossaire_juridique`
- :ref:`genindex`
- http://www.cnt-f.org/urp/
- http://fr.wikipedia.org/wiki/%C3%8Ele-de-France
- https://gitlab.com/cnt-f/iledefrance
- https://gitlab.com/cnt-f/iledefrance/-/boards


:code région: 11

L’Île-de-France (prononcé [il də fʁɑ̃s]) est une région historique et
administrative de France.

Il s'agit d'une région très fortement peuplée qui représente à elle seule 18,8 %
de la population de la France métropolitaine, ce qui en fait la région la plus
peuplée (11,73 millions d'habitants) et à la plus forte densité (976,5 hab./km²)
de France.

Ses habitants sont appelés Franciliens et Franciliennes.

Elle est fortement centralisée sur l’agglomération parisienne, qui s’étend sur
20 % de la surface régionale mais absorbe 88 % de sa population.

L’aire urbaine de Paris (qui correspond à la notion de bassin d'emploi) recouvre
quant à elle la quasi-totalité de la superficie francilienne.

- Paris (75)
- Essonne (91)
- Hauts-de-Seine (92)
- Seine-Saint-Denis (93)
- Seine-et-Marne (77)
- Val-de-Marne (94)
- Val-d'Oise (95)
- Yvelines (78)


Congrès régionaux
=================================

.. toctree::
   :maxdepth: 4

   congres_regionaux/congres_regionaux

Festival RP
============

.. toctree::
   :maxdepth: 4

   festival_rp/festival_rp


Les Unions Départementales Ile-de-France
========================================

.. toctree::
   :maxdepth: 6

   index/index
   75/75
   77/index
   78/index
   92/index
   93/93
   94/94
   95/95


Syndicats régionaux Ile de France
=================================

.. toctree::
   :maxdepth: 4

   syndicats_regionaux/syndicats_regionaux


Syndicats nationaux Ile de France
=================================

.. toctree::
   :maxdepth: 4

   syndicats_nationaux/syndicats_nationaux


Le 33 rue des Vignoles
=======================

.. toctree::
   :maxdepth: 3

   33_rue_des_vignoles/index


.. toctree::
   :maxdepth: 3

   meta/meta
