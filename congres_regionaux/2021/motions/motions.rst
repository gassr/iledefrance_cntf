

.. _motions_congres_ur_idf_2021:

==========================================================
Motions Congrès régional Ile de France 2021
==========================================================


.. toctree::
   :maxdepth: 3


   motion_1_2021_ste93
   motion_2_2021_ste93
   motion_3_2021_etpics94
