
.. index::
   pair: Prostitution ; Motion

.. _motion_ur_3_etpics94_2021:

==============================================================================
Motion N°3 **Prostitution et Abolitionnisme révolutionnaire** par ETPICS94
==============================================================================

.. seealso::

   - :ref:`etpics94`



Motion
========

Parce que la prostitution est une des plus anciennes manifestations de la
**domination patriarcale**, parce qu'elle est au cœur de la marchandisation de
l'être humain et de l'exploitation économique, parce qu'elle touche aujourd'hui
très majoritairement des personnes immigrées dans de véritables trafics atroces
et inhumains, parce qu'elle détruit les personnes jusque dans leur intimité
physique et psychologique, **la CNT affirme son positionnement abolitioniste révolutionnaire.**

**Abolitionniste**, car en tant qu'anarchosyndicalistes et syndicalistes révolutionnaires,
fidèles à nos principes et notre histoire, partisans d'une émancipation humaine
et de la construction d'une société égalitaire et libertaire, **nous ne saurions
tolérer que subsiste cette forme d'exploitation, de domination et d'aliénation
poussée à son paroxysme**.

**Révolutionnaire**, car il ne saurait s'agir de condamner ou punir ou pourchasser
via l'Etat ou des lois quelconques les victimes du système prostitutionnel, mais
parce que nous comptons détruire ce système même en combattant sans relâche
l'exploitation capitaliste, la domination sexiste, les Etats et les frontières
qui fabriquent les migrations forcées et le racisme


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°3 2021
       :ref:`ETPICS94 <etpics94>`
     - ?
     - ?
     - ?
     - ?
   * - Décision du congrès

       Acceptée ?
     -
     -
     -
     -
