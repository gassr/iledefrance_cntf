
.. index::
   pair: Groupe de Travail ; Locaux

.. _motion_ur_1_ste93_2021:

==============================================================================
Motion n° 1 Groupe de travail Locaux Motion présentée par STE 93
==============================================================================

.. seealso::

   - :ref:`etpics94`




Argumentaire
=============

La gestion au quotidien de nos locaux doit être l’une des mises en pratique
de nos principes autogestionnaires.
Cette tâche ne peut seulement reposer sur les camarades les plus régulièrement
présent·e·s au 33 et sur les permanencier·e·s.

Afin de coordonner ce travail, il est nécessaire que l’ensemble des syndicats
s’impliquent, à la hauteur de ses possibilités.

Pour cela, **nous devons recréer un groupe de travail Locaux** et définir
le mandat de ce GT Locaux.

Motion
========

**Un groupe de travail Locaux est constitué.**

Il intègre le ou la mandaté·e au calendrier ainsi que le ou la mandaté·e au
bureau des Pas Sages.

Chaque syndicat mandate au moins une personne, en particulier les syndicats
hébergés au 33 rue des Vignoles, pour participer au groupe de travail.

Afin de coordonner le travail, une liste « GT Locaux » est utilisée,
chaque syndicat y est inscrit.

Des réunions se tiennent lorsque cela est demandé par les mandaté·e·s au
groupe de travail ou par un syndicat.

Comme pour tout groupe de travail, son activité fait l’objet d’un point
à chaque réunion de l’union régionale.

Le rôle du groupe de travail est de:

- accompagner le ou la mandaté·e au bureau des Pas Sages dans la tenue
  de son mandat ;
- suivre les relations avec les autres composantes des Pas sages et avec
  la mairie de Paris ;
- organiser la gestion des espaces CNT (nettoyage, entretien du matériel,
  travaux d’aménagement...) ;
- assurer le réapprovisionnement en fournitures ;
- gérer le calendrier d’occupation des salles ;
- formuler des propositions aux syndicats de la région parisienne sur
  d’éventuels travaux d’aménagement et améliorer la gestion des locaux.

En ce qui concerne le nettoyage des espaces CNT, il n’est pas de la
responsabilité du groupe de travail de l’effectuer.

Il est proposé par le GT un calendrier dans lequel chaque syndicat de la
région doit nettoyer les locaux selon un rythme prenant en compte notamment
le nombre d’adhérent·e·s de chaque syndicat, l’éloignement géographique
et l’utilisation des locaux par chaque syndicat.

Bien entendu, chaque syndicat occupant les locaux pour une réunion interne
ou publique devra nettoyer les locaux utilisés.

De même, chaque syndicat a la responsabilité de l’organisation de ces
tâches selon les principes de la CNT : rotation des tâches, antisexisme...

Les fournitures sont prises en charge par la trésorerie régionale.

Le calendrier est géré selon la motion « Mise à disposition des Vignoles »
adoptée en 2016.

**Cette motion remplace la motion « Mandatés locaux » adoptée en 2018.**

Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°1 2021
       :ref:`STE93 <ste93>`
     - ?
     - ?
     - ?
     - ?
   * - Décision du congrès

       Acceptée ?
     -
     -
     -
     -
