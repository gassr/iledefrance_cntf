
.. index::
   pair:  Pas Sages ; 2021

.. _motion_ur_2_ste93_2021:

===========================================================================================================================
Motion n° 2 Mandatement du représentant ou de la représentante CNT au bureau des Pas Sages Motion présentée par STE 93
===========================================================================================================================

.. seealso::

   - :ref:`etpics94`




Argumentaire
==============

Les statuts de l’association des Pas Sages ainsi que le règlement intérieur
donnent un rôle primordial au bureau de l’association dont l’un·e des
quatre membres doit être désigné·e par la CNT.

Le fonctionnement que nous connaissons actuellement n’a jamais été
formalisé dans une motion alors même que ce mandat est essentiel pour
la bonne gestion de nos locaux et dans nos rapports avec la mairie de Paris
ainsi qu’avec les autres occupants du 33.

Il apparaît donc nécessaire d’adopter une motion cadrant ce mandat, sans
que cela ne soit interprété comme une remise en cause du travail effectué
jusqu’alors.

Les motions d’orientation et le contrôle des mandats font partie de notre
fonctionnement autogestionnaire.

Comme tous les mandats de la CNT, il apparaît nécessaire de le limiter
dans le temps ainsi que d’accompagner le ou la mandaté·e avec un groupe
de travail travaillant sur la question des locaux, ce GT faisant l’objet
d’une autre motion.

L’interlocutrice des Pas Sages restant l’Union Régionale Parisienne, et
donc le Bureau Régional, le ou la mandatée intègre ce dernier et utilise
les moyens de communication du BR pour échanger avec les autres structures
des Pas Sages.

Il apparaît nécessaire que le ou la mandaté·e ne soit pas adhérent·e/membre
d’une des autres associations des Pas Sages notamment au cas où cette
association et la CNT portent des positions contraires.

Il s’agit moins d’un problème de confiance que de permettre au/à la
mandaté·e de pouvoir exercer son mandat le plus sereinement possible.

Enfin, comme cela est pratiqué actuellement, un point doit être ajouté à
l’ordre du jour de chaque réunion régionale afin de faire le point, si
nécessaire, sur le fonctionnement des Pas Sages et la relation avec la
mairie.

En fonction des besoins, ce point pourra être traité en même temps que
le point plus général sur les locaux.


Motion
=======

Le congrès de l’Union Régionale Parisienne mandate un ou une adhérent·e
de la CNT pour représenter la CNT au Bureau de l’Association des Pas Sages.

Ce mandat est d’une durée d’un an, renouvelable une fois.

Le ou la mandatée ne pourra pas être adhérent·e d’une des autres associations
des Pas Sages.

Le ou la mandaté·e est membre du Groupe de Travail Locaux et du Bureau Régional.

L’adresse électronique du Bureau Régional est utilisée par le ou la mandaté·e
pour communiquer avec les autres composantes de l’association des Pas Sages.

Un point « Association des Pas Sages » est ajouté à l’ordre du jour de
chaque réunion de l’Union Régionale.

Il peut être, au besoin, confondu avec le point « GT Locaux »


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°1 2021
       :ref:`STE93 <ste93>`
     - ?
     - ?
     - ?
     - ?
   * - Décision du congrès

       Acceptée ?
     -
     -
     -
     -
