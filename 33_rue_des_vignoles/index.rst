
.. index::
   pair: 33 ; Rue des Vignoles


.. _33_rue_des_vignoles:

===============================
Le 33 rue des Vignoles
===============================



.. _motions_33_rue_des_vignoles:

Motions concernant le 33
========================

- :ref:`motion_28_2012`
- :ref:`motion_54_2012`

Souscription
============

.. seealso:: http://www.cnt-f.org/urp/actualites/215-dites-l-33-r-et-souscrivez-maintenant-


Commission 33
=============

::

	Commission locaux du 33
	33 rue des Vignoles
	75020 Paris

Histoire
=========

.. toctree::
   :maxdepth: 4

   histoire/index
