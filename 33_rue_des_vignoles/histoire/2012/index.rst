

.. _histoire_33_rue_des_vignoles_2012:

====================================
Histoire du 33 rue des Vignoles 2012
====================================



Émission du 31 août 2012 France Inter sur le 33 rue des Vignoles
=================================================================

::

	Sujet: 	[Liste-syndicats] Émission sur France Inter sur le 33 rue des Vignoles
	Date : 	Fri, 31 Aug 2012 09:53:56 +0200
	De : 	"Bureau régional de la CNT-RP" <br.rp@cnt-f.org>
	Pour : 	liste-syndicats@bc.cnt-fr.org


Camarades,

Cette après-midi a été diffusée l'émission Là bas si j'y suis, sur France
Inter, consacrée au 33 rue des Vignoles.

Vous pouvez la réécouter à cette adresse :
http://www.franceinter.fr/emission-la-bas-si-j-y-suis-le-33-rue-des-vignoles.

Pour rappel, nous invitons tou-te-s les camarades qui le peuvent à
participer à la souscription lancée pour la rénovation de nos locaux :
http://www.cnt-f.org/urp/actualites/215-dites-l-33-r-et-souscrivez-maintenant-

Salutations assr,
Commission locaux.


--
Commission locaux du 33
33 rue des Vignoles
75020 Paris
