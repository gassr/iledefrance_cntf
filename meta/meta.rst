.. index::
   ! meta-infos


.. _ile_meta_infos:

=====================
Meta infos
=====================

.. seealso::

   - https://cnt-f.gitlab.io/meta/


Gitlab project
================

.. seealso::

   - https://gitlab.com/cnt-f/iledefrance


Issues
--------

.. seealso::

   - https://gitlab.com/cnt-f/iledefrance/-/boards


pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
