
.. index::
   pair: Syndicats régionaux ; Région parisienne


.. _syndicats_regionaux_rp:

==========================================
Syndicats régionaux Région Parisienne (RP)
==========================================

.. toctree::
   :maxdepth: 4

   culture_spectacle_rp/index
   sanso_rp/index
   sii/index
   sim_rp/sim_rp
   stgl_rp/index
