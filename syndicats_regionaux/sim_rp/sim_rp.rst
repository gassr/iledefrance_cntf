
.. index::
   pair: syndicat; SIM-RP


.. _sim_rp_pub:
.. _sim_rp:
.. _simrp:

=====================================================================
SIM-RP -CNT
=====================================================================

.. seealso::

   - :ref:`union_regionale_ile_de_france`


Contact
========

L’adresse du Sim-Rp est : sim.rp@cnt-f.org.



Motions congrès
================

.. toctree::
   :maxdepth: 3

   motions_congres/motions_congres
