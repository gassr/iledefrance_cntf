
.. index::
   pair: syndicat; Culture-Spectacle-RP



.. _cs_rp:

=====================
Culture-Spectacle-RP
=====================

.. seealso::

   - :ref:`union_regionale_ile_de_france`


Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/index
