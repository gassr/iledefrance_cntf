
.. index::
   pair: Juridique ; Horaire des cadres



=====================================================================
Horaire des cadres
=====================================================================

.. seealso::

   - http://www.cnt-f.org/sii/actualites/201-les-horaires-des-cadres-une-legende-urbaine
   - :ref:`code_du_travail`


Faites un test. Demandez à dix ou vingt personnes autour de vous ce qu'il en est
du statut de cadre et écoutez leurs réponses, ce sont à peu de choses près
toujours les mêmes : « les cadres n'ont pas d'horaires », « quand tu passes
cadre, tu ne comptes plus tes heures », « pas d'heures supp' pour les cadres »
etc.

Aujourd'hui, le statut cadre est très répandu dans l'informatique, y compris
pour des postes où il n'y a personne à encadrer, mais c'est également vrai dans
tout le tertiaire et bien au-delà.

Toutefois, partout la même méconnaissance effrayante de ce statut, y compris
chez les cadres eux-mêmes. Partout la même légende urbaine qui voudrait que les
cadres n'aient pas d'horaires.

**Ceci est complètement faux**.

En réalité, seuls les cadres dits « dirigeants » travaillent hors des règles
conventionnelles.

Cette catégorie est définie par le Code du Travail, article L.3111-2 comme suit:
« Sont considérés comme ayant la qualité de cadre dirigeant les cadres auxquels
sont confiées des responsabilités dont l'importance implique une grande
indépendance dans l'organisation de leur emploi du temps, qui sont habilités à
prendre des décisions de façon largement autonome et qui perçoivent une
rémunération se situant dans les niveaux les plus élevés des systèmes de
rémunération pratiqués dans leur entreprise ou établissement ».


Autant dire que la très grande majorité des cadres n'est pas concernée.
Pour cette majorité, les lois sur le temps de travail s'appliquent comme à tous
les salariés et elles sont plutôt simples à retenir:

- la durée quotidienne  maximale de 10h (article L.3121-34),
- la durée légale hebdomadaire de 35h (article L.3121-10)
- et les durées hebdomadaires maximales de 44h en moyenne sur 12 semaines
  consécutives (article L.3121-35 et L.3121-36), c'est-à-dire le maximum en
  prenant en compte les heures supplémentaires.

Tout cadre non dirigeant bénéficie de ces réglementations, alors pourquoi ce
mythe du cadre sans horaires ?


C'est une croyance qui est probablement due à la notion de convention de forfait,
dite aussi forfait annuel. Qu'est-ce donc ?

Tout simplement une partie du Code du Travail, articles L.3121-42 et  L.3121-43
plus précisément, qui autorise certains salariés à convenir avec leur
entreprise d'un aménagement du temps de travail sous la forme d'un forfait
annuel.

En bref, les deux parties conviennent que le salarié doit effectuer une durée
de travail X sur un an, à répartir relativement librement, c'est ainsi qu'il
devient possible de travailler plus de 10h par jour ou plus de 35h par semaine,
mais attention, là encore tout n'est pas possible.

**Signer un forfait ne signifie par que toute législation disparaît**.


Pour bien comprendre, il faut tout d'abord distinguer les deux types de forfaits
existants : en heures ou en jours. La différence n'est pas négligeable, car dans
le cas d'un forfait en heures, les règles de temps de travail demeurent les mêmes,
la seule différence étant la possibilité d'aménager son emploi du temps
(par exemple 10h de travail un jour, puis 6 le lendemain).

Ainsi, la durée quotidienne maximale, la durée légale hebdomadaire et les durées
hebdomadaires maximales s'appliquent.

Ce n'est pas le cas pour le forfait en  jours, où précisément les salariés ne
sont pas soumis à ces trois réglementations  et peuvent donc effectuer
**plus de 10h par jour, entre autres**.

Alors comment un salarié peut-il savoir s'il est concerné ? Très simple :
pour être sous le régime d'un forfait, que ce soit en heures ou en jour, il
faut que le contrat de travail le stipule clairement ou qu'un avenant à ce
contrat de travail, signé par les deux parties, le stipule.

**À défaut, le salarié n'est pas au forfait**.

Le statut de cadre n'implique pas automatiquement un forfait, ainsi il est
possible de signer un avenant pour passer cadre sans que les horaires soient
modifiés.


Et les heures supplémentaires dans cette histoire ? Perdues pour les cadres ?
Pas du tout. Les cadres en bénéficient normalement, sauf les cadres dirigeants
et les cadres ayant signé un forfait en jours, qui ne peuvent pas y prétendre.

Nous sommes donc bien loin du mythe du cadre sans horaires et sans droits
(à écouter certains !).

Petit bonus à retenir : il est deux lois dont tous les cadres bénéficient, y
compris les forfaits en jours, c'est tout d'abord la pause dite « syndicale »,
de 20 minutes minimum dès que le temps de travail atteint 6 heures
(article L.3121-33), et le repos quotidien d'une durée minimale de onze heures
consécutives (article L.3131-1), qui empêche donc, par exemple, de reprendre
à 9h quand un salarié a terminé la veille à 23h.


Nous voyons bien que les certitudes répandues sur les cadres relèvent de la
superstition, née de l'incroyable faculté des salariés à méconnaître leurs droits.

Pour ne pas tout accepter de son patron, la meilleure solution reste encore de
s'informer sur le droit du travail, mais aussi sur sa convention collective
et l'accord de réduction du temps de travail de son entreprise, qui améliorent
presque systématiquement la base qu'est le Code du Travail, même dans le cas
d'une convention aussi déplorable que la SYNTEC...

Il est plus que temps que les cadres se penchent sur leurs droits, premier pas
vers une nécessaire révolte, car, quoique puisse leur souffler leur naïveté ou
leur orgueil, ce sont des travailleurs comme les autres.


::

    Guillaume
    Syndicat de l'Industrie Informatique - CNT
