
.. index::
   pair: SII RP; FAQ

==================================
Faq
==================================

.. seealso:: http://www.cnt-f.org/sii/faq


Quels sont les buts du SII ?
=============================

Nos buts sont:

- De défendre solidairement, sans subventions étatique ou patronales,
  ni démagogie, les intérêts économiques, professionnels et moraux des
  salarié-e-s dans le domaine de l'informatique, de l'internet et de la
  communication numérique.
- De développer et pratiquer des analyses et des actions pour contrecarrer
  les méthodes autoritaires et productivistes des décideurs informatiques et
  de l'Etat.
- Au delà de nos seules conditions de travail, il peut par exemple s'agir de
  promouvoir le logiciel libre, de combattre le fichage généralisé de la
  société par des moyens informatiques ou de résister à la marchandisation
  d'internet.
- De travailler à imaginer et expérimenter des façons différentes de travailler
  et de vivre, en condamnant les rapports d'autorité (patrons/salarié-e-s,
  hommes/femmes, cadres/employé-e-s...).
  Vu que la fin ne justifie pas les moyens, mais qu'au contraire les moyens
  ne doivent pas contredire les buts, notre objectif est l'élimination du
  salariat et des nomenclatures, en proposant pour les remplacer...
  d'en décider nous mêmes !

Nous ne détenons ni la lumière, ni la vérité. Nous sommes des gens qui en ont
marre et qui refusent de se résigner : nous voulons lutter, donner un sens à
notre vie, à notre société.



A quelle société rêvons-nous ?
==============================

Nous rêvons à une société ou nous soyons libres, égaux, solidaires et créatifs.

Nous rêvons à une société dont le moteur ne soit plus l'ambition du pouvoir ou
la volonté de faire du profit.

Nous ne sommes pas naïfs et ne croyons nullement  à une société parfaite qui
garantirait le bonheur absolu.

De même, nous n'attendons aucun " Grand Soir " qui réglerait soudainement tous
nos problèmes. Nous sommes simplement convaincus que notre modèle actuel
d'organisation politique et économique est loin d'être le meilleur pour la
majorité. Et nous agissons chaque jour à essayer d'en changer.

Historiquement le projet de société auquel aspirent les anarcho-syndicalistes
est le communisme libertaire, projet formalisé et couché sur le papier.

Mais nous excluons tout dogmatisme, et chaque fois que nous sommes amenés à
construire, que ce soit une grève, une façon de travailler ou éventuellement
une nouvelle société, c'est du débat et de l'expérience que sortent nos
lignes et plans.
