
.. index::
   pair: Syndicat régionale ; SII
   pair: Industrie Informatique; SII RP
   pair: twitter; https://twitter.com/#!/InformatiqueCNT
   pair: Courriel; sii.rp@cnt-f.org
   pair: Courriel; secretaire.sii.rp@cnt-f.org

.. _sii_rp:

=====================================================================
Syndicat de l'Industrie Informatique (S.I.I.), région parisienne (RP)
=====================================================================

.. seealso::

   - http://www.cnt-f.org/sii/
   - http://www.cnt-f.org/sii/qui-sommes-nous
   - https://twitter.com/#!/InformatiqueCNT

.. figure:: logo_cnt_sii_rp.png
   :align: center

   *CNT S.I.I Région Parisienne*

Adresse
=======

::

    Syndicat de l'Industrie Informatique
    33 rue des Vignoles
    75020 Paris
    Tel : 06 74 52 03 22


Adresses courriel
=================

- sii.rp@cnt-f.org
- secretaire.sii.rp@cnt-f.org


Permanence
==========

.. seealso:: http://www.cnt-f.org/sii/agenda/details/37-permanence-du-syndicat


Tous les derniers mercredi du mois, retrouvez nous lors de notre permanence de
18h à 20h, au 33 rue des Vignoles, dans la salle du fond.


Juridique
=========

.. toctree::
   :maxdepth: 4

   juridique/index

Site
=========

.. toctree::
   :maxdepth: 4

   site/index
