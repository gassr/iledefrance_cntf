
.. index::
   pair: syndicat; STGL-RP

.. _stgl_rp:

========
STGL-RP
========

.. seealso::

   - :ref:`union_regionale_ile_de_france`


Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/index
