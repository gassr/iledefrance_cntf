
.. index::
   pair: syndicat; SIPM-RP
   pair: SIPM-RP; presse.rp@cnt-f.org


.. _sipm_rp_pub:
.. _sipm_rp:
.. _sipmcs:

==================================================================================================
SIPMCS (Syndicat Interprofessionnel de la Presse des Médias Culture Spectacle, Région Parisienne)
==================================================================================================


Voir - :ref:`sipmcs:sipmcs_cnt_f`
