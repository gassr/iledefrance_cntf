
.. index::
   pair: syndicats; CNT Hauts de Seine (92)
   pair: Union départementale; UD92
   ! 92
   ! Hauts de Seine


.. _ud92_publique:
.. _ud92:

===============================================
Union Départementale Hauts de Seine (UD92)
===============================================

.. figure:: hauts_de_seine.png
   :align: center

   *Les Hauts de seine*

.. seealso::

   - http://www.cnt-f.org/spip.php?article29
   - :ref:`union_regionale_ile_de_france`


Les Hauts-de-Seine sont un département français appartenant à la petite
couronne de la région Île-de-France.

L'Insee et la Poste lui attribuent le code 92. Ses habitants sont appelés
Altoséquanais.

Adresse
=======


L'Union départementale 92 est constituée des syndicats suivants:

- :ref:`interco92 <interco92>`



Les syndicats départementaux
============================

.. toctree::
   :maxdepth: 4

   interco92/index
