
.. index::
   pair: UD94; luttes 2012
   pair: GOM; UD94

.. _luttes_ud94_2012_publique:

===============================
Luttes UD94 2012 (Val-De-Marne)
===============================

GOM
===

Le siège de cette société est situé à Sucy-en-brie, le syndicat ETPIC94
se tient à la disposition du syndicat du nettoyage pour, dans la mesure de
ses moyens, se solidariser à toute action pour obtenir l'annulation de toutes
les sanctions contre les travailleurs de GOM.

Syndicat CNT ETPIC 94

::

    LA LIBERTE COMME BASE , L'EGALITE COMME MOYEN , LA FRATERNITE COMME BUT.
