.. index::
   pair: syndicat; etpics94
   pair: etpics ; 94

.. _etpics_94_pub:
.. _etpics_94:
.. _etpics94:

====================================================================================================================================
**etpics94** Syndicat des Employés, des travailleurs et des précaires des industries, du commerce et des services du Val-de-Marne
====================================================================================================================================

.. seealso::

   - :ref:`ud94_publique`





Articles
===========

.. toctree::
   :maxdepth: 6

   articles/articles


Motions Congrès Confédéral
===========================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres



Motions Congrès Régional
===========================

.. toctree::
   :maxdepth: 4


   motions_congres_ur/motions_congres_ur
