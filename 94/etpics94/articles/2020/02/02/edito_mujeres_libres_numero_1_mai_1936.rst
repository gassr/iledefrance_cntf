.. index::
   pair: Mujeres Libres ; numero 1 mai_1936


.. _mujeres_libres_1936_05_01:

=================================================================
Editorial de **Femmes libres n° 1 - mai 1936**  (Mujeres Libres)
=================================================================





Sans prétendre être infaillibles
==================================


::

    Sin que pretendamos ser infalibles, tenemos la certeza de llegar en
    el momento oportuno.
    Ayer hubiera sido demasiado pronto; mañana, tal vez, sobrado tarde.

Sans prétendre être infaillibles, nous avons la certitude d'arriver à un
moment opportun. Hier cela aurait été trop tôt ; demain, peut-être, trop tard.

::

    Henos, pues, aquí, en plena hora nuestra, dispuestas a seguir hasta sus consecuencias últimas el
    camino que nos hemos trazado; encauzar la acción social de la mujer,
    dándola una visión nueva de las cosas, evitando que su sensibilidad
    y su cerebro se contaminen de los errores masculinos.
    Y entendemos por errores masculinos todos los conceptos actuales de relación
    y convivencia;
    errores masculinos, porque rechazamos enérgicamente toda responsabilidad
    en el devenir histórico, en el que la mujer no ha sido nunca actora,
    sino testigo obligado e inerme.


mener l'action sociale de la femme
=====================================

Nous voilà donc ici prêtes à suivre le chemin que nous nous sommes tracés jusqu'à
son ultime conséquence : **mener l'action sociale de la femme**, en lui donnant une
nouvelle vision des choses, en évitant que sa sensibilité et son cerveau se
polluent des erreurs masculines.
Et nous entendons par erreurs masculines tous les concepts actuels de relations
et de coexistence ;
erreurs masculines, parce que nous rejetons énergiquement toute responsabilité
sur le devenir historique, dans lequel la femme n'a jamais été actrice, mais
plutôt témoin obligée et sans défense.


::

    No encierra esto una recriminación para nadie; si nos duele todo el pasado
    de ignominia en que se nos tuvo hundidas, no nos atrevemos a pensar, sin embargo,
    que pudo ser de otra manera;
    sabemos que la Humanidad va haciendo su camino a costa del propio
    dolor y no nos interesa rememorar el pasado, sino forjar el presente
    y afrontar el porvenir, con la certidumbre de que en la mujer tiene la
    Humanidad su reserva suprema, un valor inédito capaz de variar, por ley
    de su propia naturaleza, todo el panorama del mundo.

Ceci ne représente aucune récrimination pour personne ; bien sûr que nous souffrons
du passé plein d'ignominie dans lequel nous étions enfouie, nous n'osons pas
penser, cependant, que cela aurait pu être autrement ;

nous savons que l'Humanité fait son chemin au détriment de sa propre douleur et il
ne nous intéresse pas de rappeler le passé, mais plutôt de façonner le présent
et d'affronter l'avenir, avec la certitude que c'est dans la femme que l'Humanité
trouve son potentiel le plus abouti, qu'elle est une valeur inédite capable de
changer, par sa nature même, la perspective du monde entier.


Réapparition du féminisme ?
===============================

::

    ¿Resurrección del feminismo? Bah! El feminismo lo mató la guerra dando a la
    mujer más de lo que pedía al arrojarla brutalmente a una forzada sustitución
    masculina.

    Feminismo que buscaba su expresión fuera de lo femenino, tratando de
    asimilarse virtudes y valores extraños no nos interesa; es otro feminismo, más sustantivo, de dentro
    a afuera, expresión de un «modo», de una naturaleza, de un complejo
    diverso frente al complejo y la expresión y la naturaleza masculinos.


Réapparition du féminisme ? Bah ! Le féminisme est tombé à la guerre,
forçant  la femme au-delà de ce qu'elle attendait en la forçant à
se lancer brutalement dans une substitution du masculin.

Feminisme qui cherchait son expression en dehors du féminin, en essayant d'assimiler
les vertus et les valeurs bizarres et qui ne nous intéresse pas ;
c'est un autre féminisme, plus substantiel, de l'intérieur vers l'extérieur,
expression d'un *mode*, d'une nature, d'un complexe autres face au complexe,
à l'expression et à la nature masculines.

::

    ¿Declaración de guerra, acaso? No, no.
    Compenetración de intereses, fusión de ansiedades, afán de cordialidad a la
    búsqueda del destino común.
    Deseo de aportar a la vida el sentido de equilibrio que le falta, y de donde
    provienen todos sus males.

Déclaration de guerre, peut-être ? Non, non.
Clairvoyance d'intérêts, fusion des angoisses, désir de convivialité à la recherche
d'un destin commun.
Envie d'apporter à la vie le sens de l'équilibre qui lui manque, et d'où viennent
tous les maux.

l'humanisme intégral
========================

::

    Pero esto es ya más que feminismo.
    Feminismo y masculinismo son dos términos de una sola
    proporción; hace algunos años un periodista francés, Leopoldo Lacour,
    halló la expresión exacta: humanismo integral.

Pour cela c'est bien plus que du féminisme.
Féminisme et masculinisme sont les deux faces de même importance ;
il y a quelques années un journaliste français, Leopold Lacour,
trouva l'expression exacte : **l'humanisme intégral**.

::

    Por falta de integridad y, consecuentemente, por falta de equilibrio,
    amenaza hundirse la civilización.
    La especie para reproducirse necesita de dos elementos, masculino y femenino;
    la sociedad es el medio en que la especié se desenvuelve, y si en la creación
    de este medio no concurren por igual los elementos antedichos, es inevitable
    que se produzca en el ser moral un desequilibrio peligroso, que puede
    llevar por caminos de ruina a la Humanidad entera.


Par manque d'intégrité et, par conséquent, par manque d'équilibre, la civilisation
risque de sombrer.
L'espèce pour se reproduire a besoin de deux éléments, masculin et féminin ;
la société est le milieu dans lequel l'espèce se développe, et si dans
la création de ce milieu les éléments considérés ne participent pas
de manière équitable, il est inévitable que se produise dans l'être
moral un déséquilibre dangereux, qui pourrait emmener l'Humanité entière à sa ruine.


Ce sont des moments décisifs pour l'Histoire
================================================

::

    He aquí la terrible encrucijada en que nos hallamos ahora.
    Exceso de audacia, de rudeza, de inflexibilidad, virtudes masculinas, han dado
    a la vida éste sentido feroz por el qué los unos se alimentan de la miseria
    y el hambre de los otros;
    la Humanidad se ha desenvuelto en dirección unilateral y esa es la consecuencia.

    La ausencia de la mujer en la Historia ha acarreado la falta de comprensión,
    de ponderación y afectividad, que son sus virtudes, y en cuyo contrapeso el
    mundo hubiera encontrado la estabilidad de que carece.

Voici la terrible croisée de chemins dans laquelle nous nous trouvons aujourd'hui.
Excès d'audace, d'insolence, d'inflexibilité, vertus masculines, ont donné à la vie
cette sensation féroce par laquelle le uns s'alimentent de la misère et la faim
des autres ;
l'Humanité s'est développée dans une seule direction et cette dernière en est la
conséquence.

L'absence de la femme dans l'Histoire a entrainé le manque de compréhension,
de pondération et d'affectivité, que sont ses vertus, mais avec ce contrepoids
le monde aurait trouvé la stabilité dont il manque.

::

    Momentos decisivos éstos para la Historia, es preciso reemprender el camino,
    rectificar errores, subvertir conceptos y, sobre todo, dar a cada cosa,
    a cada hecho, a cada manifestación humana, el valor que tiene por sí misma
    y por la intención que la produce, desligada de circunstancias o accidentes
    modificativos;
    y nadie, absolutamente nadie, puede encogerse dé hombros y permanecer ajeno
    a esa imponente tarea de gestación.

**Ce sont des moments décisifs pour l'Histoire**, il est important de reprendre le chemin,
rectifier les erreurs, renverser les concepts et, surtout, donner à chaque chose,
à chaque acte, à chaque manifestation humaine, la valeur qu'elle a et pour l'intention
qui la produit, détachée de circonstances ou d'accidents modificateurs ;
et personne, absolument personne, peut hausser les épaules et rester loin
de l'imposante tâche qu'est la gestation.


Voilà comment nait **Femmes libres**
===========================================


::

    Por esto nace MUJERES LIBRES; quiere, en este aire cargado de perplejidades,
    hacer oír una voz sincera, firme y desinteresada: la de la mujer; pero una
    voz propia, la suya, la que nace de su naturaleza íntima;
    la no sugerida ni aprendida en los coros de teorizantes; para ello tratará
    de evitar que la mujer sometida ayer a la tiranía de la religión caiga,
    al abrir los ojos a vida plena, bajo otra tiranía, no menos refinada y aun
    más brutal, que ya la cerca y la codicia para instrumento de sus ambiciones: la política.


Voilà comment nait **Femmes libres** et veut, avec cet air chargé de perplexité,
faire entendre une voix sincère, ferme et désintéressée : celle de la
femme, mais sa voix, la sienne, celle qui nait de sa nature intime ;
celle non suggérée ni apprise dans les choeurs des théoriciens ; en gardant
les yeux vivement ouverts, elle essaiera d'éviter que la femme, soumise
hier à la tyrannie de la religion, ne tombe sous une autre tyrannie,
pas moins raffinée et plus brutale encore, qui déjà l'encercle et la
convoite comme instrument de ses ambitions : la politique.


::

    La política pretende ser el arte de gobernar a los pueblos.
    Acaso sea esto en el terreno de las definiciones abstractas; pero en la
    realidad, en esa realidad que sufrimos en nuestra carne, la política es la
    podredumbre que corroe el mundo.

    Política es como decir poder, y donde hay poder hay
    esclavitud, que es relajamiento y miseria moral.

La politique prétend être l'art de gouverner les peuples.
Peut-être est-ce cela sur le terrain des définitions abstraites ; mais
effectivement, dans cette réalité dont nous souffrons dans notre propre chair,
la politique est la pourriture qui corrompt le monde.

**Politique ça veut dire pouvoir, et là où il existe du pouvoir il y a de
l'esclavage, c'est-à- dire relâchement et misère morale**.

    MUJERES LIBRES se declara por una vida libre y digna, donde cada hombre –
    empleamos esta palabra en sentido genérico –pueda ser él señor de sí mismo.

FEMMES LIBRES se déclare pour une vie libre et digne, où chaque homme -nous
employons ce mot dans le sens générique- puisse être maître de soi-même.


Les intérêts du peuple ne sont jamais les intérêts de la politique
=====================================================================

::

    MUJERES LIBRES afirma que para descubrir huevos horizontes es preciso descubrir atalayas
    nuevas.
    Nos repugna la política, porque no entiende de problemas humanos,
    sino de intereses de secta o de clase.

    Los intereses de los pueblos no son nunca los intereses de la política.
    Esta es la incubadora permanente de la guerra.

    La política lleva siempre, siempre, en sus entrañas el germen del imperialismo.
    En la política no hay rectas.
    Podría representarse por el cero mordiéndose eternamente la cola.


FEMMES LIBRES affirme que pour découvrir de nouveaux horizons il est important de
trouver de nouveaux points de mire.
La politique nous répugne, parce qu'ils ne comprennent rien aux problèmes humains,
**seulement les intérêts de secte ou de classe**.

**Les intérêts du peuple ne sont jamais les intérêts de la politique**.
C'est l'incubateur permanent de la guerre.

La politique a toujours, absolument toujours, dans ses entrailles le germe de
l'impérialisme.
En politique il n'y a pas de ligne droite.
On pourrait la représenter avec un zéro se mordant la queue à l'infini.


::

    MUJERES LIBRES busca la recta infinita de la acción directa y libre de las
    multitudes y de los individuos.
    Hay que edificar la vida nueva por procedimientos nuevos.

FEMMES LIBRES cherche l'éternelle ligne droite de l'action libre et directe
des multitudes et des individus.
Il faut construire la vie nouvelle avec de nouveaux procédés.

::

    Estamos ciertas que miles de mujeres reconocerán aquí su propia voz, y
    pronto tendremos junto a nosotras toda una juventud femenina que se agita
    desorientada en fábricas, campos y universidades, buscando afanosamente la
    manera de encauzar en fórmulas de acción sus inquietudes.

Nous sommes sûres que des milliers de femmes reconnaîtront ici leurs propres
voix, et bientôt nous aurons avec nous toute une jeunesse féminine,
désorientée et s'agitant dans les usines, les champs et universités et
qui cherche activement comment orienter ses inquiétudes en formes d'action.

CNT ETPICS 94 Traduit par Blanche.
